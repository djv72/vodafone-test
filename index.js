
/* Open index.html then view console.logs in Chrome dev tools console tab.data.
Will return a promise with an array of values dependinding on the input data (array). */
 function getProcessingPage(data) {
    return runData(data)
}
      
async function runData(data){
    var result = [];

    for(var i=0; i < data.length; i++ ){
    console.log(i,': ', data[i].state)
        
        switch(data[i].state){
            case 'success':
                    
                    result.push({title: 'Order complete', message: null});
                    break;

            case 'processing':
                    
                    await new Promise(done => setTimeout(() => done(), 2000));
                    result.push('Processing');
                    break;
              
            case 'error':
                switch (data[i].errorCode) {
                    case 'INCORRECT_DETAILS':
                        result.push({title: 'Error page', message: 'Incorrect details have been entered'});
                        break;
                    case 'NO_STOCK':
                        result.push({title: 'Error page', message: 'No stock has been found'});
                        break;
                    default:
                        result.push({title: 'Error page', message: null});
                }
                break;

            default:
                result.push(false);
        }
    }
    return result;
}


var result = getProcessingPage([{ state: 'processing'}, { state: 'error', errorCode: 'NO_STOCK' }, {state: 'success'}, { state: 'error' }, {state: 'success'}]);
console.log('Result: ', result);